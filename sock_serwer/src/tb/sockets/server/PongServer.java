package tb.sockets.server;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JFrame;


public class PongServer extends JFrame implements KeyListener, Runnable, WindowListener{


	
	private static final long serialVersionUID = 1L;
	  
	
	private static final String TITLE  = "ping-pong::server";	
	private static final int    WIDTH  = 800;
	private static final int    HEIGHT = 460;		 
	
	boolean isRunning = false;
	boolean check = true;
	
	Ball ball;
	private PlayerServer playerS;
	private PlayerClient playerC;
	
	private int ballVelocity   = 4;		
	private int playerW      = 30;		//szerokosc graczaa prostokata
	private int playerH   = 120; 	// wysokosc gracza prostokata
	private int max_Score = 4; 		// maksymalna ilosc bramek
	private int mPLAYER   = 10; 		// szybkosc
	private boolean Restart   = false;  // sprawdzenie czy koniec
	private boolean restartON = false;	//sprawdzenie czy chcemy restart
	
	private static Socket                clientSoc  = null;		
	private static ServerSocket          serverSoc  = null;
	private int portAdd;
	
	private Graphics g;
	private Font sFont = new Font("TimesRoman",Font.BOLD,90);
	private Font mFont = new Font("TimesRoman",Font.BOLD,50);
	private Font nFont = new Font("TimesRoman",Font.BOLD,32);
	private Font rFont = new Font("TimesRoman",Font.BOLD,18);
    	private String[] message;	 //dzieli wiadomosc na dwie linijki na koncu
    	private Thread ballThread;			//watek dla pilki
    
	public PongServer(String servername, String portAdd){
			
		playerS = new PlayerServer();
		playerC = new PlayerClient("");
		playerS.setName(servername);
				
		this.portAdd = Integer.parseInt(portAdd);
		this.isRunning = true;
		this.setTitle(TITLE + "::port number["+portAdd+"]");
		this.setSize(WIDTH,HEIGHT);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
			
		ball = new Ball(playerS.getBallx(),playerS.getBally(),ballVelocity,ballVelocity,45,WIDTH,HEIGHT);	//tworzymy obiekt Ball
			
		addKeyListener(this);
	   }	
		
	
	public void run() {
   	 try {
   		 	serverSoc = new ServerSocket(portAdd);
        	 playerS.setImessage("Waiting for a player...");
        	 clientSoc = serverSoc.accept();
        	
        	 if(clientSoc.isConnected()){ //sprawdzenie czy klient polaczony z serwerem
        		 
        		boolean notchecked = true; //sprawdzamy raz czy sie polaczyl zeby utworzyc watek z pilka
        		 ballThread = new Thread(ball);
        		 while(true){
        			  
        			 if(playerS.getScoreP() >= max_Score || playerS.getScoreS()>= max_Score && Restart==false){
        				 
        				 if(playerS.getScoreS()>playerS.getScoreP()){        				 
        					 playerS.setOmessage("Won               Loss-Play Again: Press any key || Exit: Esc|N");
        					 playerS.setImessage("Won               Loss-Play again? ");
        					 Restart = true;
        				 }
        				 else{
        					 playerS.setImessage("Loss              Won-Play Again: Press any key || Exit: Esc|N");
        					 playerS.setOmessage("Loss              Won-Play Again: Press any key || Exit: Esc|N");
        					 Restart = true;
        					 }
                      	ballThread.suspend();
                    }
        			 
        			
            		 if(playerC.ok && notchecked){
            			 playerS.setImessage("");
                  	    ballThread.start();
                  	    notchecked = false;
                 	}
        			 
            		 updateBall();			//aktualizacja pilki
        			
        			ObjectInputStream getObj = new ObjectInputStream(clientSoc.getInputStream());
					playerC = (PlayerClient) getObj.readObject();
					getObj = null;
					
					ObjectOutputStream sendObj = new ObjectOutputStream(clientSoc.getOutputStream());
                 	sendObj.writeObject(playerS);
                 	sendObj = null;
                 
                 	if(restartON){			//czy chcemy zrestartowac
                 	
                 		if(playerC.restart){
            			playerS.setScoreP(0);
            			playerS.setScoreS(0);
            			playerS.setOmessage("");
            			playerS.setImessage("");
            			Restart = false;
                 		playerS.setRestart(false);
                 		playerS.setBallx(380);
                 		playerS.setBally(230);
                 		ball.setX(380);
                 		ball.setY(230);
                 		ballThread.resume();
                 		restartON = false;
                 		}
                 	}
                 	repaint();
                 	}
        	}
        	 else{
        		 System.out.println("niepolaczony");
        	 }
        }
        catch (Exception e) {System.out.println(e);}
}
 
	private Image createImage(){
		
	    BufferedImage bufferedImage = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	    g = bufferedImage.createGraphics();
	    
	    g.setColor(new Color(15,9,9));		//pole gry
	    g.fillRect(0, 0, WIDTH, HEIGHT);
	    
	    
	    g.setFont(sFont);					//score
	    g.setColor(new Color(228,38,36));
	    g.drawString(""+playerS.getScoreS(), WIDTH/2-60, 120);
	    g.drawString(""+playerS.getScoreP(), WIDTH/2+15, 120);
	    
	    g.setFont(nFont);			//names
	    g.setColor(Color.white);
	    g.drawString(playerS.getName(),WIDTH/10,HEIGHT-20);
	    g.drawString(playerC.getName(),600,HEIGHT-20);
	    
	    g.setColor(new Color(57,181,74));	//prostokaty graczy
	    g.fillRect(playerS.getX(), playerS.getY(), playerW, playerH);
	    g.setColor(new Color(57,181,74));
	    g.fillRect(playerC.getX(), playerC.getY(), playerW, playerH);
	    
	    g.setColor(new Color(255,255,255));		//pilka
	    g.fillOval(playerS.getBallx(), playerS.getBally(), 45, 45);
	    
	    message = playerS.getImessage().split("-");			//wyszukiwanie piewrszego wystapienia '-' i robi nowa linijke
	    g.setFont(mFont);
	    g.setColor(Color.white);
	    if(message.length!=0){
	    g.drawString(message[0],WIDTH/4-31,HEIGHT/2+38);			//wygrany/przegrany
	    if(message.length>1){
	    	if(message[1].length()>6){
	    	    	g.setFont(rFont);
	    			g.setColor(new Color(228,38,36));
	    			g.drawString(message[1],WIDTH/4-31,HEIGHT/2+100);		//play again
	    	}
	    }
	   }
	   return bufferedImage;
	}
	
	@Override
	public void paint(Graphics g){
		g.drawImage(createImage(), 0, 0, this);
	}

	public void updateBall(){
		

		checkCol();			//sprawdzanie zdarzenie
		
		playerS.setBallx(ball.getX());		//update wspolrzednych
		playerS.setBally(ball.getY());
		
	}
	

	public void playerUP(){								//poruszanie prostokatem do gory
		if(playerS.getY() - mPLAYER > playerH/2-10){
			
			playerS.setY(playerS.getY()-mPLAYER);
		}
	}
	
	public void playerDOWN(){										//poruszanie prostokatem w dol
		if(playerS.getY() + mPLAYER < HEIGHT - playerH - 30){
			
			playerS.setY(playerS.getY()+mPLAYER);
		}
	}
	
	public void checkCol(){				//sprawdzanie kolizji
		
		
		if(playerS.getBallx() < playerC.getX() && playerS.getBallx() > playerS.getX()){
			check = true;
		}
		

		if(playerS.getBallx()>playerC.getX() && check){		//serwer gracz score
			
			playerS.setScoreS(playerS.getScoreS()+1);
			
			check = false;
		}
		
		else if (playerS.getBallx()<=playerS.getX() && check){	//client gracz score
			
			playerS.setScoreP(playerS.getScoreP()+1);
			
			check = false;
			
		}
		
		
		if(ball.getX()<=playerS.getX()+playerW && ball.getY()+ball.getRadius()>= playerS.getY() && ball.getY()<=playerS.getY()+playerH ){		//odbicie pilki od prostokata serwera
			ball.setX(playerS.getX()+playerW);
			playerS.setBallx(playerS.getX()+playerW);
			ball.setXv(ball.getXv()*-1);
		}
		
		
		// - Checking Client Player Bar - //
		if(ball.getX()+ball.getRadius()>=playerC.getX() && ball.getY() + ball.getRadius() >= playerC.getY() && ball.getY()<=playerC.getY()+playerH ){		//odbicie pilki od prostokata klienta
			ball.setX(playerC.getX()-ball.getRadius());
			playerS.setBallx(playerC.getX()-ball.getRadius());
			ball.setXv(ball.getXv()*-1);
		}
		
	}
	
	@Override
	public void keyPressed(KeyEvent arg0) {
	
		// TODO Auto-generated method stub
		int keycode = arg0.getKeyCode();
		if(keycode == KeyEvent.VK_UP){
			playerUP();
			repaint();
		}
		if(keycode == KeyEvent.VK_DOWN){
			playerDOWN();
			repaint();
		}
		if(Restart == true){
			restartON = true;
			playerS.setRestart(true);
		}
		
		if(keycode == KeyEvent.VK_N || keycode == KeyEvent.VK_ESCAPE && Restart == true){
			try {
				serverSoc.close();
				dispose();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
	
		Thread.currentThread().stop();
		try {
			serverSoc.close();
			dispose();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		System.exit(1);
	}


	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
 
}