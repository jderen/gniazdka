package tb.sockets.client;

import java.awt.Color;
import java.awt.Graphics2D;

public class Bullet {
	private double x;
	private double y;
	private int r;
	
	private double dx;
	private double dy;
	
	private double rad;
	private double speed;
	
	private Color color1;
	
	public Bullet(int x, int y, double angle)
	{
		this.x=x;
		this.y=y;
		r=5;
		speed=15;
		rad=Math.toRadians(angle);
		dx=Math.cos(rad)*speed;
		dy=Math.sin(rad)*speed;
		
		color1=Color.BLACK;
	}
	public boolean update() {
		x=x+dx;
		y=y+dy;
		
		if(x<-r||x>AnimPanel.WIDTH+r||y<-r||y>AnimPanel.HEIGHT+r) {
			return true;
		}
		
		return false;
	}
	public void draw(Graphics2D g) {
		g.setColor(color1);
		g.fillOval((int)(x-r),(int)(y-r),2*r,2*r);
	}
	
}