package sockety;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalTime;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Server extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton button_wyslij;
	private static JTextArea rozmowa;				
	private JTextField wiadomosc;				
	private static ServerSocket server;
	private static Socket socket;
	private static DataInputStream inputStream;
	private static DataOutputStream outputStream;
	private static String message;
	private String messageAfterButton;
	private int klawisz;
	private static LocalTime czas;
	private JScrollPane scroll;
	
	
	public static void main(String[]args) {
		message="";	
		EventQueue.invokeLater(new Runnable(){				
			public void run(){
				new Server();
			}
		});
	
				try {
					server=new ServerSocket(3333);
					socket=server.accept();
					inputStream= new DataInputStream(socket.getInputStream());
					outputStream=new DataOutputStream(socket.getOutputStream());
					
					while(true) {
						czas=LocalTime.now();
						message=inputStream.readUTF();
						rozmowa.append("Client: "+czas.getHour() + ":" + czas.getMinute()+ ":" +czas.getSecond()+"\n"+message+"\n");
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					inputStream.close();
					outputStream.close();
					server.close();
					socket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
	public Server() {
		
		ActionListener al=new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(e.getSource()==button_wyslij) {
					try {
					czas=LocalTime.now();
					messageAfterButton="";
					messageAfterButton=wiadomosc.getText();//trim
					outputStream.writeUTF(messageAfterButton);
					rozmowa.append("Server: "+czas.getHour() + ":" + czas.getMinute()+ ":" +czas.getSecond()+"\n"+messageAfterButton+"\n");
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		};
		
		
		KeyListener kl=new KeyListener() {

			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				klawisz=arg0.getKeyCode();
				if(klawisz==KeyEvent.VK_ENTER) {
					try {
						czas=LocalTime.now();
						messageAfterButton="";
						messageAfterButton=wiadomosc.getText();
						outputStream.writeUTF(messageAfterButton);
						rozmowa.append("Server: "+czas.getHour() + ":" + czas.getMinute()+ ":" +czas.getSecond()+"\n"+messageAfterButton+"\n");	
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
		};
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Server");
		setLayout(null);
		
		button_wyslij=new JButton("wyslij");
		button_wyslij.setBounds(350, 450, 100, 30);
		button_wyslij.addKeyListener(kl);
		button_wyslij.addActionListener(al);
		
		rozmowa=new JTextArea();
		
		scroll=new JScrollPane(rozmowa);
		scroll.setBounds(20,20,760,300);
		
		wiadomosc=new JTextField();
		wiadomosc.setBounds(20,370,760,30);
		

		
		
		add(button_wyslij);
		add(scroll);
		add(wiadomosc);
		
		
		
		setSize(800,500);
		setVisible(true);
	}
}
